package anagrams

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import Anagrams._

@RunWith(classOf[JUnitRunner])
class AnagramsSuite extends FunSuite  {

  test("wordOccurences: \"\"") {
    assert(wordOccurrences("") === List())
  }

  test("wordOccurrences: abcd") {
    assert(wordOccurrences("abcd") === List(('a', 1), ('b', 1), ('c', 1), ('d', 1)))
  }

  test("wordOccurrences: Robert") {
    assert(wordOccurrences("Robert") === List(('b', 1), ('e', 1), ('o', 1), ('r', 2), ('t', 1)))
  }

  test("wordOccurrences: 23104") {
    assert(wordOccurrences("23104") === List(('0', 1), ('1', 1), ('2', 1), ('3', 1), ('4', 1)))
  }

  test("sentenceOccurrences: \"\"") {
    assert(sentenceOccurrences(List()) === List())
  }

  test("sentenceOccurrences: abcd e") {
    assert(sentenceOccurrences(List("abcd", "e")) === List(('a', 1), ('b', 1), ('c', 1), ('d', 1), ('e', 1)))
  }

  test("sentenceOccurrences: Hi Mom") {
    assert(sentenceOccurrences(List("Hi", "Mom")) === List(('h', 1), ('i', 1), ('m', 2), ('o', 1)))
  }

  test("dictionaryByOccurrences.get: eat") {
    assert(dictionaryByOccurrences.get(List(('a', 1), ('e', 1), ('t', 1))).map(_.toSet) === Some(Set("ate", "eat", "tea")))
  }

  test("word anagrams: asfeg") {
    assert(wordAnagrams("asfeg").toSet === Set())
  }

  test("word anagrams: married") {
    assert(wordAnagrams("married").toSet === Set("married", "admirer"))
  }

  test("word anagrams: player") {
    assert(wordAnagrams("player").toSet === Set("parley", "pearly", "player", "replay"))
  }

  test("subtract: lard - r") {
    val lard = List(('a', 1), ('d', 1), ('l', 1), ('r', 1))
    val r = List(('r', 1))
    val lad = List(('a', 1), ('d', 1), ('l', 1))
    assert(subtract(lard, r) === lad)
  }

  test("subtract: larrd - r") {
    val lard = List(('a', 1), ('d', 1), ('l', 1), ('r', 2))
    val r = List(('r', 1))
    val lad = List(('a', 1), ('d', 1), ('l', 1), ('r', 1))
    assert(subtract(lard, r) === lad)
  }

  test("subtract: lard - lard") {
    val lard1 = List(('a', 1), ('d', 1), ('l', 1), ('r', 1))
    val lard2 = List(('a', 1), ('d', 1), ('l', 1), ('r', 1))
    val lad = List()
    assert(subtract(lard1, lard2) === lad)
  }

  test("subtract: lady - \"\"") {
    val lady = List(('a', 1), ('d', 1), ('l', 1), ('y', 1))
    val emptyList = List()
    assert(subtract(lady, emptyList) === lady)
  }

  test("subtract: \"\" - \"\" ") {
    val emptyList = List()
    val emptyList2 = List()
    assert(subtract(emptyList, emptyList2) == List())
  }

  test("subtract: zero character ") {
    val emptyList = List(('a', 0))
    val emptyList2 = List()
    assert(subtract(emptyList, emptyList2) == List())
  }

  test("subtract: Invalid negative character ") {
    val emptyList = List(('a', -1))
    val emptyList2 = List()
    assert(subtract(emptyList, emptyList2) == List())
  }

  test("combinations: []") {
    assert(combinations(Nil) === List(Nil))
  }

  test("combinations: a") {
    assert(combinations(List(('a', 1))) === List(List(), List(('a', 1))))
  }

  test("combinations: aaa") {
    assert(combinations(List(('a', 3))) === List(List(), List(('a', 1)), List(('a', 2)), List(('a', 3))))
  }

  test("combinations: abba") {
    val abba = List(('a', 2), ('b', 2))
    val abbacomb = List(
      List(),
      List(('a', 1)),
      List(('a', 2)),
      List(('b', 1)),
      List(('a', 1), ('b', 1)),
      List(('a', 2), ('b', 1)),
      List(('b', 2)),
      List(('a', 1), ('b', 2)),
      List(('a', 2), ('b', 2))
    )
    assert(combinations(abba).toSet === abbacomb.toSet)
  }

  test("sentence anagrams: []") {
    val sentence = List()
    assert(sentenceAnagrams(sentence) === List(Nil))
  }

  test("sentence anagrams: Invalid") {
    val sentence = List("123", "3")
    assert(sentenceAnagrams(sentence).toSet === List().toSet)
  }

  test("sentence anagrams: zzz") {
    val sentence = List("zzz")
    assert(sentenceAnagrams(sentence).toSet === List().toSet)
  }

  test("sentence anagrams: tan") {
    val sentence = List("tan")
    val anas = List(
      List("ant"),
      List("Nat"),
      List("tan")
    )
    assert(sentenceAnagrams(sentence).toSet === anas.toSet)
  }
  test("sentence anagrams: I love you") {
    val sentence = List("I", "love", "you")
    val anas = List(
      List("Lev", "you", "Io"),
      List("Io", "you", "Lev"),
      List("Io", "Lev", "you"),
      List("you", "Io", "Lev"),
      List("you", "olive"),
      List("you", "Lev", "Io"),
      List("Lev", "Io", "you"),
      List("olive", "you")
    );
    assert(sentenceAnagrams(sentence).toSet === anas.toSet)
  }

  test("sentence anagrams: Linux rulez") {
    val sentence = List("Linux", "rulez")
    val anas = List(
      List("Rex", "Lin", "Zulu"),
      List("nil", "Zulu", "Rex"),
      List("Rex", "nil", "Zulu"),
      List("Zulu", "Rex", "Lin"),
      List("null", "Uzi", "Rex"),
      List("Rex", "Zulu", "Lin"),
      List("Uzi", "null", "Rex"),
      List("Rex", "null", "Uzi"),
      List("null", "Rex", "Uzi"),
      List("Lin", "Rex", "Zulu"),
      List("nil", "Rex", "Zulu"),
      List("Rex", "Uzi", "null"),
      List("Rex", "Zulu", "nil"),
      List("Zulu", "Rex", "nil"),
      List("Zulu", "Lin", "Rex"),
      List("Lin", "Zulu", "Rex"),
      List("Uzi", "Rex", "null"),
      List("Zulu", "nil", "Rex"),
      List("rulez", "Linux"),
      List("Linux", "rulez")
    )
    assert(sentenceAnagrams(sentence).toSet === anas.toSet)
  }
}
